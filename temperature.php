<?php $titre ="température";
session_start();

$nombassin = "inconnu";
$idbassin = 0 ;
//isset permet de savoir si oui ou non il y a la presence d'une variable ou d'un element
$idok = isset($_GET['idbassin']);
$nomok = isset($_GET['nombassin']);

if (($idok == true) && ($nomok == true)) {
   $nombassin = htmlspecialchars($_GET["nombassin"]);
   $idbassin = intval(htmlspecialchars($_GET["idbassin"]));
}
// requete SQL
require 'bdd/bddconfig.php';
$objBdd = new PDO("mysql:host=$bddserver;
dbname=$bddname;
charset=utf8",
$bddlogin, $bddpass);
$listeTemperatures = $objBdd->prepare("SELECT * FROM temperature WHERE idBassin = :id ORDER BY date DESC");
$listeTemperatures->bindParam(':id', $idbassin, PDO::PARAM_INT);
$listeTemperatures->execute();
?>

<?php ob_start(); ?>
<article>                
    <h1>température : <?php echo $nombassin; ?></h1>
    <table>
        <thead>
            <tr>
                <th>Date</th>
                <th>Température (°C)</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($listeTemperatures as $temps){ ?>
            <tr>
                <td><?php echo $temps['date']; ?></td>
                <td><?php echo $temps['temp']; ?></td>
            </tr>
            
            <?php } $listeTemperatures->closeCursor() ?>
        </tbody>
    </table>
            </article>
            <?php $contenu = ob_get_clean(); ?>            
<?php require 'gabarit/template.php' ?>;