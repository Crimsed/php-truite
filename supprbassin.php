<?php if ((!isset($_POST['idBassin']))) {
    // Code de supprBassin 
?>
    <?php $titre = "Supprimer un bassin"; ?>
<?php require 'bdd/bddconfig.php'; ?>
<?php ob_start();
session_start(); 

if (isset($_SESSION['logged_in']['login']) !== TRUE) {
    // Redirige vers la page d'accueil (ou login.php) si pas authentifié
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
}

?>
<?php
if (isset($_GET["idbassin"])) {
    $idbassin = intval(htmlspecialchars($_GET["idbassin"]));
}

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bassins = $objBdd->query("select * from bassin");

} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

?>
<article>     
    <h1>Suppression d'un bassin</h1>
    <table>
        <thead>
            <tr>
                <th>Bassin</th>
                <th>Suppression</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bassins as $bassin) { ?>
                <tr>
                    <td><?php echo $bassin['nom']; ?></td>
                    <td>
                        <form method="POST" action="deletebassin.php">
                            <input type="hidden" name="idbassin" value="<?php echo $bassin['idBassin']; ?>">
                            <input type="submit" value="Supprimer">
                        </form>
                    </td>
                </tr>
                <?php
            } //fin foreach
            $bassins->closeCursor(); //libère les ressources de la bdd
            ?>
        </tbody>
    </table>
</article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>
<?php } else {
    // Code de deletebassin 
?>
    <?php
require 'bdd/bddconfig.php'; 
$paramOK = false;

if (isset($_POST["idbassin"])) {
    $idbassin = intval(htmlspecialchars($_POST["idbassin"]));
    $paramOK = true;
}

if ( $paramOK == true ) {
try {
    $objBdd = new PDO("mysql:host=$bddserver;
    dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //supp les températures de ce bassin
    $RSlogins = $objBdd->prepare("DELETE FROM temperature WHERE idBassin = :id");
    $RSlogins-> bindParam(':id',$idbassin,PDO::PARAM_INT);
    $RSlogins->execute();

    //supprimer le bassin de la table bassin
    $RSlogins = $objBdd->prepare("DELETE FROM bassin WHERE idBassin = :id");
    $RSlogins-> bindParam(':id',$idbassin,PDO::PARAM_INT);
    $RSlogins->execute();

    // redirige vers una page différente du dossier courant
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';
    header("Location: http://$serveur$chemin/$page");
    
}catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
}
else{
    die('erreur');
}

?>
<?php } ?>