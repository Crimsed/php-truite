<?php $titre = "Supprimer un bassin"; ?>
<?php require 'bdd/bddconfig.php'; ?>
<?php ob_start(); ?>
<?php
if (isset($_GET["idbassin"])) {
    $idbassin = intval(htmlspecialchars($_GET["idbassin"]));
}

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bassins = $objBdd->query("select * from bassin");

} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}






?>
<article>     
    <h1>Suppression d'un bassin</h1>
    <table>
        <thead>
            <tr>
                <th>Bassin</th>
                <th>Suppression</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bassins as $bassin) { ?>
                <tr>
                    <td><?php echo $bassin['nom']; ?></td>
                    <td>
                        <form method="POST" action="deletebassin.php">
                            <input type="hidden" name="idbassin" value="<?php echo $bassin['idBassin']; ?>">
                            <input type="submit" value="Supprimer">
                        </form>
                    </td>
                </tr>
                <?php
            } //fin foreach
            $bassins->closeCursor(); //libère les ressources de la bdd
            ?>
        </tbody>
    </table>
</article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>