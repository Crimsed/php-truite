<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $titre; ?></title>
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>
        <div id="conteneur">
            <header>
            <?php
            
            if (isset($_SESSION['logged_in']['login']) == TRUE) {
            //l'internaute est authentifié
            echo $_SESSION['logged_in']['prenom'].' '.$_SESSION['logged_in']['nom'];
            //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
            ?>
            <a  href="logout.php">déconnection</a>
            <?php
            } else { 
            //Personne n'est authentifié 
            // affichage d'un lien pour se connecter
            ?>
            <a  href="login.php" id="login">Connexion</a>
            
            <?php               
}?>
            <h1>La pisciculture PHP</h1>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="bassins.php">Les bassins</a></li>
                    <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>

                <?php   if (isset($_SESSION['logged_in']['login']) == TRUE) {
                //l'internaute est authentifié
                //affichage des liens vers les pages privées
                ?>

                <li><a href="ajouterunbassin.php">ajouter un bassin</a></li>
                <li><a href="supprbassin.php">supprimer un bassin</a></li>
                <?php
                }?>
                </ul>
            </nav>
            <section>
              <?php echo $contenu; ?>
            </section>

            <footer>
                <p>Copyright TruitesPHP - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>