<?php $titre ="bassin"; ?>
<?php 
require "bdd/bddconfig.php";
session_start();
try {
    $objBdd = new PDO("mysql:host=$bddserver;
   dbname=$bddname;
   charset=utf8",$bddlogin, $bddpass);

    $objBdd->setAttribute(PDO::ATTR_ERRMODE,
   PDO::ERRMODE_EXCEPTION);

   $listeBassins = $objBdd->query("SELECT * FROM bassin");

   }
catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
   }
?>
<?php ob_start(); ?>
    <article>                
        <h1>Les bassins</h1>

        <?php
        //fetch = parcourir dans la fonction
        while ($bassin = $listeBassins->fetch()) {
        //$bassin signifie que la fonction s'appel bassin, il faut toujours 
        //mettre $ avant une fonction
        ?>

        <h2><?php echo $bassin['nom']; ?></h2>
        <p><?php echo $bassin['description']; ?></p>
        <img src="images/<?php echo $bassin['photo'] ?>" alt="">
        <p><a href="temperature.php?idbassin=<?php echo $bassin['idBassin'] ?>&nombassin=<?php echo $bassin['nom'] ?>">voir les températures</a></p>
        <?php
        } //fin du while

        $listeBassins->closeCursor(); //libère les ressources de la bdd
        ?>

        </article>
        <?php $contenu = ob_get_clean(); ?>       
<?php require 'gabarit/template.php';?>