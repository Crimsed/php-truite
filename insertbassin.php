<?php 
require "bdd/bddconfig.php";
if (isset($_POST["nom"])){
    $nom = htmlspecialchars($_POST["nom"]);
        if(isset($_POST["descript"])) {
            $descript = htmlspecialchars($_POST["descript"]);
            if(isset($_POST["refcapteur"])){
                $refcapteur = htmlspecialchars($_POST["refcapteur"]);
                $paramOK = true;
            }
        }
}

if ($paramOK == true) {
    try {
        $objBdd = new PDO("mysql:host=$bddserveur;
        dbname=$bddname;
        charset=utf8",$bddlogin,$bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $listeTemperatures = $objBdd->prepare("INSERT INTO bassin(nom, description, refCapteur) VAlUES (:nom,:descript, :refcapteur)");
        $listeTemperatures->bindParam(':nom', $nom, PDO::PARAM_STR);
        $listeTemperatures->bindParam(':descript', $descript, PDO::PARAM_STR);
        $listeTemperatures->bindParam(':refcapteur', $refcapteur, PDO::PARAM_STR);
        $listeTemperatures->execute();

        $lastId = $objBdd->lastInsertId();
} 
catch (Exception $prme) {
    die ('erreur : ' . $prme->getMessage());
}

$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$page = 'bassins.php';
header("Location: http://$serveur$chemin/$page");

}
else{
    die('erreur');
}

?>