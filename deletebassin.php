<?php
require 'bdd/bddconfig.php'; 
$paramOK = false;

if (isset($_POST["idbassin"])) {
    $idbassin = intval(htmlspecialchars($_POST["idbassin"]));
    $paramOK = true;
}

if ( $paramOK == true ) {
try {
    $objBdd = new PDO("mysql:host=$bddserver;
    dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //supp les températures de ce bassin
    $RSlogins = $objBdd->prepare("DELETE FROM temperature WHERE idBassin = :id");
    $RSlogins-> bindParam(':id',$idbassin,PDO::PARAM_INT);
    $RSlogins->execute();

    //supprimer le bassin de la table bassin
    $RSlogins = $objBdd->prepare("DELETE FROM bassin WHERE idBassin = :id");
    $RSlogins-> bindParam(':id',$idbassin,PDO::PARAM_INT);
    $RSlogins->execute();

    // redirige vers una page différente du dossier courant
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';
    header("Location: http://$serveur$chemin/$page");
    
}catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
}
else{
    die('erreur');
}

?>